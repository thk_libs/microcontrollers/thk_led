## **Projekttitel:** 
**LEDController und LEDChain**  
  
* LEDs Signale erzeugen ohne Nutzung von delay()
* Verwendung mehrerer LEDs parallel

![grafik-aufbau.png](./grafik-aufbau.png)  
## **Beschreibung:** 
Sollen mehrere LEDs, z.B. zur Anzeige von Statusinformationen, in unterschiedlicher Frequenz gleichzeitig blinken, so kann dieses Repo geeignet sein.

## **Installation:** 
Um diese Klassen verwenden zu können, muss diese Repository geklont und in das Library-Verzeichnis der Arduino-IDE kopiert werden.<br />
<br />

## **Anwendung:** 
Einbinden der Library:
```Arduino
#include "led_controller.h"
```
<br />

## **Erläuterungen:** 

**Klasse LEDController:**
* `void blink(uint16_t interval)`: Lasse die LED blinken mit der übergebenen Intervallzeit.
* `void on()`: Einschalten
* `void off()`: Ausschalten
* `void toggle()`: Umschalten
* `void switch_to(bool)`: Schalten
* `byte read_pin_state()`: Lesen den aktuellen Zustand der LED

**Klasse LEDChain:**
* `LEDChain(byte aPin_count, byte aPin_chain[], Chainmode chainmode = RIGHT)`: Erzeuge ein ledchain-Objekt (Konstruktor)  
`aPin_count`: Größe des Pin-Arrays  
`aPin_chain[]`: Pin-Array  
`chainmode`:  
    RIGHT=von rechts nach links (default)  
    LEFT= von links nach rechts  
    TOGGLE=von links nach rechts nach links nach ...  

* `void run(uint16_t interval)`: Lasse Lauflicht laufen mit übergebener Intervallzeit.
* `void set_chainmode(Chainmode cm)`: Nachträgliches Ändern des Lauflichtes  
`cm`:  
    RIGHT=von rechts nach links (default)  
    LEFT= von links nach rechts  
    TOGGLE=von links nach rechts nach links nach ... 
* `void on()`: Alle LED einschalten.
* `void off()`: Alle LED ausschalten.
